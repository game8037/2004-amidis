<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_doc extends CI_Model {

    var $table_name = "doc";
    var $pk = "docid";

    function getAll() {
        $query = $this->db->get($this->table_name);
        return $query->result();
    }
    
    function getAllBy($kondisi) {
        $query = $this->db->get_where($this->table_name, $kondisi);
        return $query->result();
    }
    
    function getAllByQuery($query) {
        $query = $this->db->query($query);
        return $query->result();
    }
    
    function getDetail($id) {
        $this->db->where($this->pk, $id);
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function add($data) {
        $this->db->insert($this->table_name, $data);
    }
    
    function update($id, $data) {
        $this->db->where($this->pk, $id);
        $this->db->update($this->table_name, $data);
    }
    
    function delete($id) {
        $this->db->where($this->pk, $id);
        $this->db->delete($this->table_name);
    }
    
    function getMax($data) {
        $this->db->select_max($data);
        $query = $this->db->get($this->table_name);
        return $query->result()[0]->$data;
    }
}