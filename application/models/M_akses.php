<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_akses extends MY_Model {

    var $table_name = "user_menu";
    var $view = "
    SELECT * FROM (
    (
    SELECT
        u.userid,
        um.usermenuid,
        u.fullname,
        m.menuid menu,
        m.name namamenu 
    FROM
        user_menu um
        LEFT JOIN sys_user u ON u.userid = um.userid
        LEFT JOIN mst_menu m ON um.menuid = m.menuid 
        ) UNION
        (
    SELECT
        u.userid,
        um.usermenuid,
        u.fullname,
        m2.menuid menu,
        m2.name namamenu 
    FROM
        user_menu um
        LEFT JOIN sys_user u ON u.userid = um.userid
        LEFT JOIN mst_menu m ON um.menuid = m.menuid
        LEFT JOIN mst_menu m2 ON m.parentid = m2.menuid 
        ) UNION
        (
    SELECT
        u.userid,
        um.usermenuid,
        u.fullname,
        m3.menuid menu,
        m3.name namamenu 
    FROM
        user_menu um
        LEFT JOIN sys_user u ON u.userid = um.userid
        LEFT JOIN mst_menu m ON um.menuid = m.menuid
        LEFT JOIN mst_menu m2 ON m.parentid = m2.menuid
        LEFT JOIN mst_menu m3 ON m2.parentid = m3.menuid 
        ) UNION
        (
    SELECT
        u.userid,
        um.usermenuid,
        u.fullname,
        m4.menuid menu,
        m4.name namamenu 
    FROM
        user_menu um
        LEFT JOIN sys_user u ON u.userid = um.userid
        LEFT JOIN mst_menu m ON um.menuid = m.menuid
        LEFT JOIN mst_menu m2 ON m.parentid = m2.menuid
        LEFT JOIN mst_menu m3 ON m2.parentid = m3.menuid 
        LEFT JOIN mst_menu m4 ON m3.parentid = m4.menuid 
        ) UNION
        (
    SELECT
        u.userid,
        um.usermenuid,
        u.fullname,
        m5.menuid menu,
        m5.name namamenu 
    FROM
        user_menu um
        LEFT JOIN sys_user u ON u.userid = um.userid
        LEFT JOIN mst_menu m ON um.menuid = m.menuid
        LEFT JOIN mst_menu m2 ON m.parentid = m2.menuid
        LEFT JOIN mst_menu m3 ON m2.parentid = m3.menuid 
        LEFT JOIN mst_menu m4 ON m3.parentid = m4.menuid 
        LEFT JOIN mst_menu m5 ON m4.parentid = m5.menuid 
	) 
	) as akses
    ";

    function getByQuery($query) {
        $query = $this->db->query($this->view." WHERE ".$query);
        return $query->result();
    }
}