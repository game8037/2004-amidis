<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class M_usermenu extends MY_Model {

    var $table_name = "user_menu";
    var $pk = "usermenuid";
}