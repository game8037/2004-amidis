<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require(APPPATH.'/libraries/REST_Controller.php');

class Menu extends REST_Controller {
	
	var $kelas = "Api/Menu";
	var $title = "menu";

	function __construct(){
		parent::__construct();
		// if (!$this->session->userdata("id")){
		// 	redirect("Login");
		// }
		// $id = $this->session->userdata("id");
		// $this->user = $this->M_user->getDetail($id);

	}

	function index_get($methode = "", $param1 ="")
    {
        if($param1 == "")
        {
            $user = $this->M_menu->getAll();
            $this->response($user, 200);
        }
 
        $user = $this->M_menu->getDetail( $param1 );
         
        if($user)
        {
            $this->response($user, 200); // 200 being the HTTP response code
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }
     
}
