<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		 if (!$this->session->userdata("id")){
		 	redirect("Login");
		 }
		// $this->load->model("M_kategori");
//				$this->session->set_userdata("id",1);
	}

	public function index()
	{
		$data["konten"] = "dashboard";
		$this->load->view('template',$data);
	}
}
