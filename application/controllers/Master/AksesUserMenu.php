<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AksesUserMenu extends CI_Controller {
	
	var $kelas = "Master/AksesUserMenu";
	var $title = "Akses User";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}
		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);

	}

	public function index($userid){
		$data = array(
				"rowData" => $this->M_usermenu->getAll(),
				"userData" => $this->M_user->getDetail($userid),
                "rowMenu" => $this->M_usermenu->getAllBy("userid = $userid"),

				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "master/akses",
			);
		$this->load->view('template',$data);
	}

	public function add(){
        if($this->input->post("btnSubmit")){
            $data["userid"] = $userid = $this->input->post("tuserid");
			$data["menuid"] = $this->input->post("tmenuid");

            $this->M_usermenu->add($data);
		}

		redirect($this->kelas."/index/".$userid);
	}

	public function update(){
		if($this->input->post("btnSubmit")){
			$id = $this->input->post("tid");
            $data["userid"] = $userid = $this->input->post("tuserid");
            $data["menuid"] = $this->input->post("tmenuid");

			$this->M_usermenu->update($id,$data);
		}

		redirect($this->kelas."/index/".$userid);
	}

	public function delete($id){
        $akses = $this->M_usermenu->getDetail($id);
        $this->M_usermenu->delete($id);
        redirect($this->kelas."/index/".$akses->userid);
	}
}
