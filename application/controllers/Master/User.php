<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	
	var $kelas = "Master/User";
	var $title = "User";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}
		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);

	}

	public function index(){
		$data = array(
				"rowData" => $this->M_user->getAll(),
				
				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "master/user",
			);
		$this->load->view('template',$data);
	}

	public function add(){
		if($this->input->post("btnSubmit")){
			$data["username"] = $this->input->post("tusername");
			$data["nickname"] = $this->input->post("tnickname");
			$data["fullname"] = $this->input->post("tfullname");
            $data["password"] = $this->input->post("tpassword") ? $this->encrypt->encode($this->input->post("tpassword")) : $this->encrypt->encode("asd");

            $this->M_user->add($data);
		}

		redirect($this->kelas);
	}

	public function update(){
		if($this->input->post("btnSubmit")){
			$id = $this->input->post("tid");
			$password = $this->input->post("tpassword");
            $data["username"] = $this->input->post("tusername");
            $data["nickname"] = $this->input->post("tnickname");
            $data["fullname"] = $this->input->post("tfullname");
            if($password != "")
                $data["password"] = $this->encrypt->encode($this->input->post("tpassword"));

			$this->M_user->update($id,$data);
		}

		redirect($this->kelas);
	}

	public function delete(){	
		$id = $this->input->post("tid");
		$this->M_user->delete($id);
		redirect($this->kelas);
	}
}
