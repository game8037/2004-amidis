<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {
	
	var $kelas = "Master/Menu";
	var $title = "Menu";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}
		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);

	}

	public function index(){
		$data = array(
				"rowData" => $this->M_menu->getAll(), 
				
				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "master/menu", 
			);
		$this->load->view('template',$data);
	}

	public function add(){
		if($this->input->post("btnSubmit")){
			$level = 1;
			$parentid = $this->input->post("tparentid");
			if($parentid){
				$parent = $this->M_menu->getDetail($parentid);
				$level = $parent->level + 1;
			}
			$data["level"] = $level;
			$data["parentid"] = $parentid;
			// $data["categoryid"] = $this->input->post("tcategoryid");
			$data["name"] = $this->input->post("tname");
			$this->M_menu->add($data);
		}

		redirect($this->kelas);
	}

	public function update(){
		if($this->input->post("btnSubmit")){
			$level = 1;
			$parentid = $this->input->post("tparentid");
			if($parentid){
				$parent = $this->M_menu->getDetail($parentid);
				$level = $parent->level + 1;
			}
			$data["level"] = $level;
			$data["parentid"] = $parentid;
			 $id = $this->input->post("tid");
			$data["name"] = $this->input->post("tname");
			$this->M_menu->update($id,$data);
		}

		redirect($this->kelas);
	}

	public function delete(){	
		$id = $this->input->post("tid");
		$this->M_docmenu->deleteBy("menuid = ".$id);
		$this->M_menu->delete($id);
		redirect($this->kelas);
	}

	// API
	public function apiDetail($id)
	{
		var_dump($this->M_menu->getDetail($id));
	}
}
