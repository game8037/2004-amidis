<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {
	
	var $kelas = "Master/Department";
	var $title = "Department";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}
		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);

	}

	public function index(){
		$data = array(
				"rowData" => $this->M_department->getAll(), 
				
				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "master/department", 
			);
		$this->load->view('template',$data);
	}

	public function add(){
		if($this->input->post("btnSubmit")){
			$data["code"] = $this->input->post("tcode");
			$data["name"] = $this->input->post("tname");
			$data["parentid"] = $this->input->post("tparentid");
			$this->M_department->add($data);
		}

		redirect($this->kelas);
	}

	public function update(){
		if($this->input->post("btnSubmit")){
			$id = $this->input->post("tid");
			$data["code"] = $this->input->post("tcode");
			$data["name"] = $this->input->post("tname");
			$data["parentid"] = $this->input->post("tparentid");
			$this->M_department->update($id,$data);
		}

		redirect($this->kelas);
	}

	public function delete(){	
		$id = $this->input->post("tid");
		$this->M_department->delete($id);
		redirect($this->kelas);
	}
}
