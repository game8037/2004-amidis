<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {
	
	var $kelas = "Document/Upload";
	var $title = "Upload";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}
		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);
		$this->time = unix_to_human(now(), TRUE, 'eu');

	}

	public function index(){
		$dataUpdate = array(
					"docid" => "",
					"code" => "",
					"filename" => "",
					"name" => "",
					"deptid" => "",
					"type" => "",
					"desc" => "",
					"menuid" => "",
					"fileuri" => "",
		);
		$data = array(
				"rowData" => $this->M_department->getAll(), 
				"data" => (object)$dataUpdate,

				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "document/upload", 
			);
		$this->load->view('template',$data);
	}

	public function add(){
        if($this->input->post("btnSubmit")){
            try{
                $menuid = $this->input->post("menuid");
                $data = array(
							// "code" => $this->input->post("code"),
//							"docid" => "",
							"name" => $this->input->post("name"),
							"deptid" => $this->input->post("deptid"),
							"type" => $this->input->post("type"),
							"desc" => $this->input->post("desc"),
							"fileuri" => $this->input->post("fileuri"),
//							"menuid" => $menuid,
							"created_by" => $this->user->userid,
							"created_at" => $this->time,
				);

                $this->M_doc->add($data);
                $docid = $this->M_doc->getMax("docid");

                if($menuid){
                    $data = array(
                        "docid" => $docid,
                        "menuid" => $menuid,
                    );
                    $this->M_docmenu->add($data);
                }
//	        	$docname = $_FILES['doc']['name'];
//
//		        if ($_FILES['doc']['name']) {
//		            $uploadPath = "extras/upload/document/";
//		            $filename = $this->unggah->upload_files('doc', "", $uploadPath);
//		            // $filename = $this->unggah->upload_files('doc', $docname, $uploadPath);
//		            $filetype = $_FILES['doc']['type'];
                $this->session->set_flashdata("success","Data Berhasil disimpan");
//
//					$dataUpdate = array(
//								"filename" => str_replace(" ", "_", $filename),
//								"filetype" => $filetype,
//					);
//					$this->M_doc->update($docid,$dataUpdate);
//
//		        } else {
//		            $filename = '';
//		            $filetype = '';
//					$this->session->set_flashdata("warning","File gagal disimpan");
//		            $this->M_doc->delete($docid);
//		        }
            }
            catch(Exception $e){
                $this->session->set_flashdata("warning",$e);
            }
        }
        if($menuid)
            redirect($this->kelas."/update/".$docid."/Document/Display/Menu/".$menuid);
        else
            redirect("Document/Display/Menu/0");
    }

	public function update($docid, $uri1, $uri2, $uri3, $uri4){
		$uri = $uri1."/".$uri2."/".$uri3."/".$uri4;


		if($this->input->post("btnSubmit")){
			$dataUpdate = array(
						// "code" => $this->input->post("code"),
						 "docid" => $docid,
						"name" => $this->input->post("name"),
						"deptid" => $this->input->post("deptid"),
						"type" => $this->input->post("type"),
						"desc" => $this->input->post("desc"),
						"menuid" => $this->input->post("menuid"),
                        "fileuri" => $this->input->post("fileuri"),
			);

			$this->M_doc->update($docid,$dataUpdate);
			$this->session->set_flashdata("success","Data Berhasil disimpan");
			redirect($uri);
		}

		$data = array(
				"rowData" => $this->M_department->getAll(), 
				"data" => $this->M_doc->getDetail($docid),
				"rowMenu" => $this->M_docmenu->getAllBy("docid = $docid"),
				"uri" => $uri,
				
				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "document/upload", 
			);
		$this->load->view('template',$data);
	}

	public function addMenu($docid, $uri1, $uri2, $uri3, $uri4){

		$data = array(
					"docid" => $docid,
					"menuid" => $this->input->post("dupmenuid"),
		);

		$this->M_docmenu->add($data);	

		$uri = $uri1."/".$uri2."/".$uri3."/".$uri4;
		redirect($this->kelas."/update/$docid/$uri");
	}

	public function delete($docid, $uri1, $uri2, $uri3, $uri4){	
		$uri = $uri1."/".$uri2."/".$uri3."/".$uri4;
		$this->session->set_flashdata("success","Data Berhasil dihapus");
		$this->M_doc->delete($docid);
		$this->M_docmenu->delete($docid);
		redirect($uri);
	}

	public function deleteMenu($menuid, $docid, $uri1, $uri2, $uri3, $uri4){	
		$uri = $uri1."/".$uri2."/".$uri3."/".$uri4;
		$this->session->set_flashdata("success","Menu 'Linked to' Berhasil dihapus");
		$this->M_docmenu->delete($menuid);
		redirect($this->kelas."/update/$docid/$uri");
	}
}
