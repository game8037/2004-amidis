<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display extends CI_Controller {
	
	var $kelas = "Document/Display";
	var $title = "Display";

	function __construct(){
		parent::__construct();
		if (!$this->session->userdata("id")){
			redirect("Login");
		}
		$id = $this->session->userdata("id");
		$this->user = $this->M_user->getDetail($id);

	}

	public function index(){
		$data = array(
				"rowData" => $this->M_department->getAll(), 
				
				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "document/display", 
			);
		$this->load->view('template',$data);
	}

	public function Menu($menuid = 0){
	    if($menuid) {
	        $rowData = $this->M_docmenu->getAllBy("menuid = '$menuid'");
	        $namadokumen = $this->M_menu->getDetail($menuid)->name;
        }
	    else {
	        $query = "SELECT d.docid 
                      FROM
                        doc d
                        LEFT JOIN doc_menu dm ON d.docid = dm.docid 
                      WHERE
                        dm.menuid IS NULL OR
                        dm.menuid = 0";
	        $rowData = $this->M_doc->getAllByQuery($query);
	        $namadokumen = "TEMPORARY";
        }
		$data = array(
				 "rowData" => $rowData,
//				"rowData" => $this->M_doc->getAllByQuery($query),
				
				"title" => $this->title, 
				"subtitle" => "Document ".$namadokumen,
				"kelas" => $this->kelas, 
				"konten" => "document/display", 
			);
		$this->load->view('template',$data);
	}

	public function Dept($type, $deptid){
		$data = array(
				"rowData" => $this->M_doc->getAllBy("type = '$type' AND deptid = '$deptid'"), 
				
				"title" => $this->title, 
				"subtitle" => (($type == "sop")?"SOP":(($type == "dta")?"DATA":"Dokumentasi"))." - ".$this->M_department->getDetail($deptid)->code, 
				"kelas" => $this->kelas, 
				"konten" => "document/display", 
			);
		$this->load->view('template',$data);
	}

	public function Preview($id){
		$data = array(
				"data" => $this->M_doc->getDetail($id), 
				
				"title" => $this->title, 
				"kelas" => $this->kelas, 
				"konten" => "document/preview", 
			);
		$this->load->view('template',$data);
	}

	public function add(){
		if($this->input->post("btnSubmit")){
			$data["code"] = $this->input->post("tcode");
			$data["name"] = $this->input->post("tname");
			$data["parentid"] = $this->input->post("tparentid");
			$this->M_department->add($data);
		}

		redirect($this->kelas);
	}

	public function update(){
		if($this->input->post("btnSubmit")){
			$id = $this->input->post("tid");
			$data["code"] = $this->input->post("tcode");
			$data["name"] = $this->input->post("tname");
			$data["parentid"] = $this->input->post("tparentid");
			$this->M_department->update($id,$data);
		}

		redirect($this->kelas);
	}

	public function delete($docid, $menuid){
//		$id = $this->input->post("tid");
        if($menuid == 0)
            $this->M_doc->delete($docid);
		else
            $this->M_docmenu->deleteBy("docid = $docid AND menuid = $menuid");

		redirect($this->kelas.'/Menu/'.$menuid);
	}
}
