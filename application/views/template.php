<?php
    $iduser = $this->session->userdata("id");
    $user = $this->M_user->getDetail($iduser);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.6/admin/html/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Feb 2015 05:09:34 GMT -->
<head>
  <meta charset="utf-8" />
  <title>DMS | Amidis</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  
  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <!-- <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" /> -->
  <link href="<?=base_url('extras/');?>plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/animate.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/style.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/style-responsive.min.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>css/theme/default.css" rel="stylesheet" id="theme" />
  <!-- ================== END BASE CSS STYLE ================== -->

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
  
  <!-- ================== BEGIN datatable STYLE ================== -->
  <link href="<?=base_url('extras/');?>plugins/DataTables/css/data-table.css" rel="stylesheet" />
  <!-- ================== END datatable STYLE ================== -->
  
  <!-- ================== BEGIN validation STYLE ================== -->
  <link href="<?=base_url('extras/');?>plugins/parsley/src/parsley.css" rel="stylesheet" />
  <!-- ================== END validation STYLE ================== -->
  
  <!-- ================== BEGIN dashboard STYLE ================== -->
  <link href="<?=base_url('extras/');?>plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap-datepicker/css/datepicker.css" rel="stylesheet" />
  <link href="<?=base_url('extras/');?>plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
  <!-- ================== END dashboard STYLE ================== -->

  <!-- ================== BEGIN BASE JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/pace/pace.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery/jquery-1.9.1.min.js"></script>
  <!-- ================== END BASE JS ================== -->
  
  <!-- ================== BEGIN Vue JS ================== -->
  <script src="<?=base_url('extras/');?>js/vue.js"></script>
  <script src="<?=base_url('extras/');?>js/vue.min.js"></script>
  <!-- ================== END validation JS ================== -->
</head>
<body>
  <!-- begin #page-loader -->
  <div id="page-loader" class="fade in"><span class="spinner"></span></div>
  <!-- end #page-loader -->
  
  <!-- begin #page-container -->
    <?php $this->load->view("partial/header"); ?>
    <!-- end #header -->
    
    <!-- begin #sidebar -->
    <?php $this->load->view("partial/menu"); ?>
    <!-- end #sidebar -->
    
    <!-- begin #content -->
    <div id="content" class="content">
      <?php $this->load->view("page/$konten"); ?>    
    </div>
    <!-- end #content -->
    
    <!-- begin scroll to top btn -->
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    <!-- end scroll to top btn -->
  </div>
  <!-- end page container -->
  
  <!-- ================== BEGIN BASE JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/bootstrap/js/bootstrap.min.js"></script>
  <!--[if lt IE 9]>
    <script src="<?=base_url('extras/');?>crossbrowserjs/html5shiv.js"></script>
    <script src="<?=base_url('extras/');?>crossbrowserjs/respond.min.js"></script>
    <script src="<?=base_url('extras/');?>crossbrowserjs/excanvas.min.js"></script>
  <![endif]-->
  <script src="<?=base_url('extras/');?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery-cookie/jquery.cookie.js"></script>
  <!-- ================== END BASE JS ================== -->

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

  <!-- ================== BEGIN datatable JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/DataTables/js/jquery.dataTables.js"></script>
  <script src="<?=base_url('extras/');?>js/table-manage-default.demo.min.js"></script>
  <!-- ================== END datatable JS ================== -->

  <!-- ================== BEGIN validation JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/parsley/dist/parsley.js"></script>
  <!-- ================== END validation JS ================== -->

  <!-- ================== BEGIN dashboard JS ================== -->
  <script src="<?=base_url('extras/');?>plugins/gritter/js/jquery.gritter.js"></script>
  <script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.time.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.resize.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/flot/jquery.flot.pie.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/sparkline/jquery.sparkline.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="<?=base_url('extras/');?>plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <script src="<?=base_url('extras/');?>plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script src="<?=base_url('extras/');?>js/dashboard.min.js"></script>
  <script src="<?=base_url('extras/');?>js/apps.min.js"></script>
  <!-- ================== END dashboard JS ================== -->
  
  <script>
    $(document).ready(function() {
      App.init();
      Dashboard.init();
      TableManageDefault.init();
    });

    $('.selectpicker').selectpicker();
  </script>
</html>

