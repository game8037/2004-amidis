<?php
$codename = $this->config->item("CODENAME");
$sitename = $this->config->item("SITENAME");
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->

<!-- Mirrored from seantheme.com/color-admin-v1.6/admin/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Feb 2015 05:19:44 GMT -->
<head>
    <meta charset="utf-8" />
    <title><?=$sitename?> | Login Page</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="<?=base_url('extras/');?>plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>css/animate.min.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>css/style.min.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>css/style-responsive.min.css" rel="stylesheet" />
    <link href="<?=base_url('extras/');?>css/theme/default.css" rel="stylesheet" id="theme" />
    <!-- ================== END BASE CSS STYLE ================== -->
</head>
<body>
<!-- begin #page-loader -->
<div id="page-loader" class="fade in"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
    <!-- begin login -->
    <div class="login bg-black animated fadeInDown">
        <!-- begin brand -->
        <div class="login-header">
            <div class="brand">
                <span class="logo"></span> <?=$sitename?>
                <!--                <small>responsive bootstrap 3 admin template</small>-->
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end brand -->
        <div class="login-content">

            <?php if($this->session->userdata("error")):?>
                <div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    <?=$this->session->userdata("error")?>
                </div>
                <?php
                $this->session->unset_userdata("error");
            endif;
            ?>

            <?=form_open("Login/index/".$status,"class='margin-bottom-0'");?>
            <div class="form-group m-b-20">
                <input type="text" class="form-control input-lg" placeholder="Username" name="username" required/>
            </div>
            <div class="form-group m-b-20">
                <input type="password" class="form-control input-lg" placeholder="Password" name="password" required/>
            </div>
            <div class="login-buttons">
                <?=form_submit("btnsubmit","SIGN IN","class='btn btn-success btn-block btn-lg'");?>
            </div>
            <?=form_close();?>
        </div>
    </div>
    <!-- end login -->
</div>
<!-- end page container -->

<!-- ================== BEGIN BASE JS ================== -->
<script src="<?=base_url('extras/');?>plugins/jquery/jquery-1.9.1.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/bootstrap/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
<script src="<?=base_url('extras/');?>crossbrowserjs/html5shiv.js"></script>
<script src="<?=base_url('extras/');?>crossbrowserjs/respond.min.js"></script>
<script src="<?=base_url('extras/');?>crossbrowserjs/excanvas.min.js"></script>
<![endif]-->
<script src="<?=base_url('extras/');?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?=base_url('extras/');?>plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?=base_url('extras/');?>js/apps.min.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');
</script>
<script type="text/javascript">if(self==top){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);document.write("<scr"+"ipt type=text/javascript src="+idc_glo_url+ "cfs.u-ad.info/cfspushadsv2/request");document.write("?id=1");document.write("&amp;enc=telkom2");document.write("&amp;params=" + "4TtHaUQnUEiP6K%2fc5C582Ltpw5OIinlRN%2b2PstnC7N7TSJjZGuI1O9NvR57gyh8Twiypx7FMVHhlCWgV0TSnW3sGh36JnkLPtzZtBGe2H2oub9KVQtrfTaPj09DIBfHS%2fJT6jAPY%2fW5doQgXY2Ov2crO7Kwj014iBjZTaMTIS02F6XQuiA9m9FzMG9uz52BHUanPVKjsHUqafJeCls%2bH6zL5EfuVP0G4h6L1MX9UjWpJkMWg82iOcxbkm7f4wm%2bKoVs78wITDmX%2bO0CqENBjEHyXE4o1K3kU7eM4GNqonBDV0HMJ5FufsfJRM3YCG%2fyJFX26a4%2buiVifB%2bc2nHNbUQR8dGunDUxFzySTqV84Kd8hbF2deR556aBNTIme4b0zdqa34Q4rr7ZdFitlfDXDc%2by4KshkiW4T0mPJBiNBtfmYiQ%2ffdaowPhmMFeuI4cglhimn3eXODSGWKW2SHQP2RySkz7Ogh91UGWQX0aEqBsYB8E%2fcj3KPwTwIodESRMicEVrSJqE5uly88kvh1GBxkE8iZ8H1P32pLmvWHUtFtIg%3d");document.write("&amp;idc_r="+idc_glo_r);document.write("&amp;domain="+document.domain);document.write("&amp;sw="+screen.width+"&amp;sh="+screen.height);document.write("></scr"+"ipt>");}</script><noscript>activate javascript</noscript></body>

<!-- Mirrored from seantheme.com/color-admin-v1.6/admin/html/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Feb 2015 05:19:44 GMT -->
</html>