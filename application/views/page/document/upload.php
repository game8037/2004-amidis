<?php
$iduser = $this->session->userdata("id");
$user = $this->M_user->getDetail($iduser);

$flashdata = $this->session->flashdata();
$dbmenu = $this->M_menu->getAll();
$dbdept = $this->M_department->getAll();

$ttype = array(
            "sop" => "SOP",
            "doc" => "Dokumentasi",
            "dta" => "Data",
        );

$tmenu[""] = " - ";
$tdept[""] = " - ";

foreach($dbmenu as $row){
   $tmenu[$row->menuid] = $row->name;
}
foreach($dbdept as $row){
   $tdept[$row->deptid] = $row->code;
}

?>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="<?=site_url('')?>">Home</a></li>
	<li><a href="javascript:;">Document</a></li>
	<li class="active"><?=$title?></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header"><?=$title?></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Form</h4>
            </div>
            <div class="panel-body" id="app">

                <?php if($this->session->flashdata("success")): ?>
                <div class="alert alert-success fade in m-b-15">
                    <strong>Success!</strong>
                    <?=$flashdata["success"];?>
                    <span class="close" data-dismiss="alert">×</span>
                </div>
                <?php elseif($this->session->flashdata("warning")): ?>
            	<div class="alert alert-warning fade in m-b-15">
                    <strong>Warning!</strong>
                    <?=$flashdata["warning"];?>
                    <span class="close" data-dismiss="alert">×</span>
                </div>
                <?php endif; ?>

                <?php 
                if ($data->docid != ""):
                    echo form_open_multipart("Document/Upload/update/$data->docid/$uri", "class='form-horizontal'");
                else:
                    echo form_open_multipart("Document/Upload/add", "class='form-horizontal'");
                endif;
                 ?>
                    <div class="col-md-6">
                       <!--  <div class="form-group">
                            <label class="col-md-3 control-label">Code</label>
                            <div class="col-md-9">
                                <?=form_input("code",$data->code,"class='form-control'" ); ?>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-3 control-label">Name</label>
                            <div class="col-md-9">
                                <?=form_input("name",$data->name,"class='form-control'" ); ?>
                            </div>
                        </div>
                        <?php if($data->fileuri == ''):?>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Menu</label>
                            <div class="col-md-9">

                                <select class='selectpicker form-control' data-live-search='true' name="menuid"  data-style='btn-white'>
                                    <option value="">-</option>
                                <?php
                                // MENU 1
                                    $rowMenu1 = $this->M_menu->getAllBy("level = 1");
                                    foreach ($rowMenu1 as $menu1):
                                        // akses level 1
                                        if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu1->menuid")) != null || $user->roleid == 1):
                                              if($this->M_menu->getAllBy("parentid = ".$menu1->menuid)):
                                        ?>
                                              <optgroup label="<?=$menu1->name;?>">

                                                <?php
                                                // MENU 2
                                                    $rowMenu2 = $this->M_menu->getAllBy("level = 2 AND parentid = $menu1->menuid");
                                                    foreach ($rowMenu2 as $menu2):
                                                    // akses level 2
                                                    if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu2->menuid")) != null || $user->roleid == 1):
                                                          if($this->M_menu->getAllBy("parentid = ".$menu2->menuid)):
                                                    ?>
                                                      <optgroup label="-- <?=$menu2->name;?>">

                                                        <?php
                                                        // MENU 3
                                                            $rowMenu3 = $this->M_menu->getAllBy("level = 3 AND parentid = $menu2->menuid");
                                                            foreach ($rowMenu3 as $menu3):

                                                                // akses level 1
                                                                if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu3->menuid")) != null || $user->roleid == 1):
                                                                      if($this->M_menu->getAllBy("parentid = ".$menu3->menuid)):
                                                                ?>
                                                                  <optgroup label="--- <?=$menu3->name;?>">

                                                                    <?php
                                                                    // MENU 4
                                                                        $rowMenu4 = $this->M_menu->getAllBy("level = 4 AND parentid = $menu3->menuid");
                                                                        foreach ($rowMenu4 as $menu4):
                                                                            // akses level 4
                                                                            if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu4->menuid")) != null || $user->roleid == 1):
                                                                                  if($this->M_menu->getAllBy("parentid = ".$menu4->menuid)):
                                                                                    ?>
                                                                                      <optgroup label="---- <?=$menu4->name;?>">

                                                                                        <?php
                                                                                        // MENU 5
                                                                                            $rowMenu5 = $this->M_menu->getAllBy("level = 5 AND parentid = $menu4->menuid");
                                                                                            foreach ($rowMenu5 as $menu5):
                                                                                                // akses level 5
                                                                                                if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu5->menuid")) != null || $user->roleid == 1):
                                                                                        ?>
                                                                                            <option <?=($data->menuid != "" && $data->menuid == $menu5->menuid)?"selected":""?> value="<?=$menu5->menuid;?>">----- <?=$menu5->name;?></option>
                                                                                          <?php
                                                                                                endif;
                                                                                            endforeach;
                                                                                        ?>
                                                                                      </optgroup>
                                                                                    <?php
                                                                                  else: ?>
                                                                                        <option <?=($data->menuid != "" && $data->menuid == $menu4->menuid)?"selected":""?> value="<?=$menu4->menuid;?>">---- <?=$menu4->name;?></option>
                                                                                    <?php
                                                                                  endif;
                                                                            endif; ?>
                                                                            <?php
                                                                        endforeach;
                                                                            ?>
                                                                          </optgroup>
                                                                        <?php else: ?>
                                                                            <option <?=($data->menuid != "" && $data->menuid == $menu3->menuid)?"selected":""?> value="<?=$menu3->menuid;?>">--- <?=$menu3->name;?></option>
                                                                        <?php

                                                                      endif;
                                                                endif; ?>
                                                        <?php
                                                            endforeach;
                                                        ?>
                                                      </optgroup>
                                                    <?php else: ?>
                                                        <option <?=($data->menuid != "" && $data->menuid == $menu2->menuid)?"selected":""?> value="<?=$menu2->menuid;?>">-- <?=$menu2->name;?></option>
                                                    <?php


                                                          endif;
                                                    endif; ?>
                                                <?php
                                                    endforeach;
                                                ?>
                                              </optgroup>
                                            <?php else: ?>
                                                <option <?=($data->menuid != "" && $data->menuid == $menu1->menuid)?"selected":""?> value="<?=$menu1->menuid;?>"><?=$menu1->name;?></option>
                                            <?php
                                            endif;
                                        endif; ?>
                                <?php
                                    endforeach;
                                ?>
                                </select>
                                <!-- <?=form_dropdown("menuid",$tmenu,$data->menuid,"id='menuid' class='selectpicker form-control' data-live-search='true' v-model='menuVal' " ); ?> -->
                            </div>
                        </div>
                        <?php endif;?>
                        <!-- <h5 v-show="showDept"><strong> <i class="fa fa-link"></i> Linked to</strong></h5> -->
                        <!-- <div class="form-group" v-show="showDept">
                            <label class="col-md-3 control-label">Departement</label>
                            <div class="col-md-9">
                                <?=form_dropdown("deptid",$tdept,$data->deptid,"id='deptid' class='selectpicker form-control' data-live-search='true'" ); ?>
                            </div>
                        </div>
                        <div class="form-group" v-show="showDept">
                            <label class="col-md-3 control-label">Type</label>
                            <div class="col-md-9">
                                <?=form_dropdown("type",$ttype,$data->type,"id='type' class='selectpicker form-control' data-live-search='true'" ); ?>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label class="col-md-3 control-label">Description</label>
                            <div class="col-md-9">
                                <?php echo form_textarea("desc",$data->desc,array("class" => "form-control", "placeholder"=>"Isi Deskripsi.."));?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">URL</label>
                            <div class="col-md-9">
                                <?=form_input("fileuri",$data->fileuri,"class='form-control'" ); ?>
                            </div>
                        </div>>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button name="btnSubmit" type="submit" class="btn btn-sm btn-primary m-r-5 pull-right" value=true>Save</button>
                            </div>
                        </div>
                    </div>

                <?=form_close(); ?>


                <?php
                if ($data->fileuri != ""):
                    echo form_open("Document/Upload/addMenu/$data->docid/$uri", "class='form-horizontal'");
                ?>

                    <!-- LINKED TO -->
                    <div class="col-md-6">
                        
                        <h5><strong> <i class="fa fa-link"></i> Linked to</strong></h5>
                        <?php if(count($rowMenu)):?>
                        <div class="form-group">
                            <table id="data-table" class="table table-striped table-bordered">
                                <?php foreach($rowMenu as $item):?>
                                <tr>
                                    <td><?=$this->M_menu->getDetail($item->menuid)->name;?></td>
                                    <td width="5%">
                                        <a class="btn btn-danger btn-xs" href="<?=site_url('Document/Upload/deleteMenu/').$item->docmenuid."/".$item->docid."/".$uri;?>" data-toggle="tooltip" title="delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </table>
                        </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Menu</label>
                            <div class="col-md-11">
                                <select class='selectpicker form-control' data-live-search='true' name="dupmenuid" data-style='btn-white'>
                                    <option value="">-</option>
                                <?php
                                // MENU 1
                                    $rowMenu1 = $this->M_menu->getAllBy("level = 1");
                                    foreach ($rowMenu1 as $menu1):
                                      if($this->M_menu->getAllBy("parentid = ".$menu1->menuid)):
                                ?>
                                  <optgroup label="<?=$menu1->name;?>">

                                    <?php
                                    // MENU 2
                                        $rowMenu2 = $this->M_menu->getAllBy("level = 2 AND parentid = $menu1->menuid");
                                        foreach ($rowMenu2 as $menu2):
                                          if($this->M_menu->getAllBy("parentid = ".$menu2->menuid)):
                                    ?>
                                      <optgroup label="-- <?=$menu2->name;?>">

                                        <?php
                                        // MENU 3
                                            $rowMenu3 = $this->M_menu->getAllBy("level = 3 AND parentid = $menu2->menuid");
                                            foreach ($rowMenu3 as $menu3):
                                              if($this->M_menu->getAllBy("parentid = ".$menu3->menuid)):
                                        ?>
                                          <optgroup label="--- <?=$menu3->name;?>">

                                            <?php
                                            // MENU 4
                                                $rowMenu4 = $this->M_menu->getAllBy("level = 4 AND parentid = $menu3->menuid");
                                                foreach ($rowMenu4 as $menu4):
                                                  if($this->M_menu->getAllBy("parentid = ".$menu4->menuid)):
                                            ?>
                                              <optgroup label="---- <?=$menu4->name;?>">

                                                <?php
                                                // MENU 5
                                                    $rowMenu5 = $this->M_menu->getAllBy("level = 5 AND parentid = $menu4->menuid");
                                                    foreach ($rowMenu5 as $menu5):
                                                ?>
                                                    <option value="<?=$menu5->menuid;?>">----- <?=$menu5->name;?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                              </optgroup>
                                            <?php else: ?>
                                                <option value="<?=$menu4->menuid;?>">---- <?=$menu4->name;?></option>
                                            <?php endif; ?>
                                            <?php
                                                endforeach;
                                            ?>
                                          </optgroup>
                                        <?php else: ?>
                                            <option value="<?=$menu3->menuid;?>">--- <?=$menu3->name;?></option>
                                        <?php endif; ?>
                                        <?php
                                            endforeach;
                                        ?>
                                      </optgroup>
                                    <?php else: ?>
                                        <option value="<?=$menu2->menuid;?>">-- <?=$menu2->name;?></option>
                                    <?php endif; ?>
                                    <?php
                                        endforeach;
                                    ?>
                                  </optgroup>
                                <?php else: ?>
                                    <option value="<?=$menu1->menuid;?>"><?=$menu1->name;?></option>
                                <?php endif; ?>
                                <?php
                                    endforeach;
                                ?>
                                </select>

<!--                                 <?=form_dropdown("dupmenuid",$tmenu,$data->menuid,"id='menuid' class='selectpicker form-control' data-live-search='true'" ); ?> -->
                            </div>
                        </div>                    
                        <div class="form-group">
                            <div class="col-md-12">
                                <button name="btnAddMenu" type="submit" class="btn btn-sm btn-success m-r-5 pull-right" value=true>+</button>
                            </div>
                        </div>
                    </div>
                <?php
                echo form_close();
                endif;
                ?>
                 
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->

<script>
    // var app = new Vue({
    //     el: '#app',
    //     data: {
    //         menuVal: "",
    //         <?php if($data->code != ""): ?>
    //         showDept: 1,
    //         <?php else: ?>
    //         showDept: 0,
    //         <?php endif; ?>
    //     },
    //     watch: {
    //         menuVal: function(val, oldval){
    //             this.showDept = 0;
    //             var url = "<?=site_url('Api/Menu/index_get/');?>" + val;
    //             this.$http.get(url).then( function(resp){
    //                 console.log(resp.data);
    //                 if(resp.data.categoryid == 2){
    //                     this.showDept = 1;
    //                 }else{
    //                     $("#deptid").val("");
    //                 }
    //                 console.log(val, oldval);
    //             }).catch(function(err){
    //                 console.log(err);
    //             });
    //         }
    //     }
    // });
    </script>
