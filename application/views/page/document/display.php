<?php 
$uri = "/".$this->uri->segment(1)."/".$this->uri->segment(2)."/".$this->uri->segment(3)."/".$this->uri->segment(4)."/".$this->uri->segment(5);
$menuid = $this->uri->segment(4);
 ?>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="<?=site_url('')?>">Home</a></li>
	<li><a href="javascript:;">Document</a></li>
	<li class="active"><?=$title?></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header"><?=$title?></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title"><?=$subtitle?></h4>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <!-- <th>Document Code</th> -->
                                <th>Document Name</th>
                                <?php if($menuid != 0):?>
                                <th>Linked To</th>
                                <?php endif;?>
                                <th>Created At</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $no = 1;
                            foreach ($rowData as $row): 
                                $doc = $this->M_doc->getDetail($row->docid);
                                $rowMenu = $this->M_docmenu->getAllBy("docid = $row->docid");
                                ?>
                                <tr>
                                    <td><?=$no++;?></td>
                                    <!-- <td><?=$doc->code;?></td> -->
                                    <td><?=$doc->name;?></td>
                                    <?php if($menuid != 0):?>
                                    <td>
                                        <?php
                                        if($rowMenu):
                                        foreach($rowMenu as $item):
                                            if($item->menuid != $menuid):?>
                                            <label for="" class="label label-primary"><?=$this->M_menu->getDetail($item->menuid)->name?></label>
                                        <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                    </td>
                                    <?php endif;?>
                                    <td><?=date("d-m-Y", strtotime($doc->created_at));?></td>
                                    <td>
                                        <a class="btn btn-success btn-xs" href="<?=$doc->fileuri;?>" data-toggle="tooltip" title="download"><i class="fa fa-download"></i></a>
                                        <a class="btn btn-warning btn-xs" href="<?=site_url('Document/Upload/update/').$doc->docid.$uri;?>" data-toggle="tooltip" title="edit"><i class="fa fa-pencil"></i></a>
                                        <?php if($this->uri->segment(4) == 0):?>
                                            <a class="btn btn-danger btn-xs" href="<?=site_url('Document/Upload/delete/').$doc->docid.$uri;?>" data-toggle="tooltip" title="delete"><i class="fa fa-trash"></i></a>
                                        <?php else: ?>
                                            <a class="btn btn-danger btn-xs" href="<?=site_url('Document/Display/delete/').$doc->docid.'/'.$this->uri->segment(4);?>" data-toggle="tooltip" title="delete"><i class="fa fa-trash"></i></a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->