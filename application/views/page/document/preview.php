<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="<?=site_url('')?>">Home</a></li>
	<li><a href="javascript:;">Document</a></li>
	<li class="active"><?=$title?></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header"><?=$title?></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Preview</h4>
            </div>
            <div class="panel-body" style="height: 450px">
            	<div class="row">
                    <div class="col-md-10">
                        <?php if($data->filetype == "application/pdf"): ?>
                            <object data="<?=site_url('extras/upload/document/').$data->filename;?>" type="application/pdf" width="100%" height="400px" >
                                <p>Alternative text - include a link <a href="~/Extras/uploads/@ViewBag.materi.text">to the PDF!</a></p>
                            </object>
                        <?php elseif($data->filetype == "image/jpeg" || $data->filetype == "image/jpg" || $data->filetype == "image/png" || $data->filetype == "image/gif"): ?>
                            <img src="<?=site_url('extras/upload/document/').$data->filename;?>" alt="" height="400px">
                        <?php else: ?>
                            <div class="note note-warning">
                                <h4>File tidak didukung pratinjau.</h4>
                                <p>
                                    File tidak dapat dilihat, kemungkinan file ini bukan image atau pdf, silahkan download untuk melihat isi file. 
                                        <a class="btn btn-success btn-xs" href="<?=site_url('extras/upload/document/').$data->filename;?>" data-toggle="tooltip" title="download"><i class="fa fa-download"></i> download</a> 
                                </p>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->