<?php

$arrDept[""] = "== Pilih $title ==";  
foreach ($rowData as $row) {
    $arrDept[$row->deptid] = $row->code;  
}
?>



<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="<?=site_url('')?>">Home</a></li>
	<li><a href="javascript:;">Master Data</a></li>
	<li class="active"><?=$title?></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header"><?=$title?></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">List</h4>
            </div>
            <div class="panel-body">
			    <div>
						<a class="btn btn-primary" href="#modal-add" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span>Add New</span>
						</a>
						<div>&nbsp;</div>
				</div>
                <div class="table-responsive">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
				              <th width="10%">No</th>
				              <th>Code</th>
				              <th>Name</th>
				              <th width="15%">action</th>
                            </tr>
                        </thead>
                        <tbody>
						  <?php 
			              $no = 1;
			              foreach ($rowData as $row) :
			              ?>
			              <tr>
			                <td><?=$no++;?></td>
			                <td><?=$row->code;?></td>
			                <td><?=$row->name;?></td>
			                <td class="text-center">
			                <span data-toggle="tooltip" data-placement="top" title="edit">
			                  <a href="#modal-detail" class="btn btn-xs btn-warning getDetail" data-toggle="modal" data-action="update" data-id="<?=$row->deptid;?>" data-code="<?=$row->code;?>" data-name="<?=$row->name;?>"><i class="fa fa-pencil"></i> </a>
			                </span>
			                <span data-toggle="tooltip" data-placement="top" title="delete">
			                  <a href="#modal-delete" class="btn btn-xs btn-danger getDetail" data-toggle="modal" data-action="delete" data-id="<?=$row->deptid;?>" data-code="<?=$row->code;?>" data-name="<?=$row->name;?>"> <i class="fa fa-trash"></i> </a>
			                </span>
			                </td>
			              </tr>
			              <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->





<!-- #modal-add -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<?php echo form_open("$kelas/add","class='form-horizontal' data-parsley-validate='true'");?>
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-white">Add <?=$title?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Code * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tcode","","class='form-control' data-parsley-required='true'");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Name * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tname","","class='form-control' data-parsley-required='false'");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Parent Code * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tparentid",$arrDept,"-","class='form-control tparentid' data-parsley-required='false'");?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<?php echo form_submit("btnSubmit","SUBMIT","class='btn btn-sm btn-primary'");?>
				</div>
            <?php echo form_close();?>
		</div>
	</div>
</div>

<!-- #modal-detail -->
<div class="modal fade" id="modal-detail">
	<div class="modal-dialog">
		<div class="modal-content">
			<?php echo form_open("$kelas/add","class='form-horizontal formDetail' data-parsley-validate='true'");?>
			<?php echo form_input("tid","","class='form-control tid hidden'");?>
				<div class="modal-header bg-orange">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-white">Update <?=$title?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Code * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tcode","","class='form-control tcode' data-parsley-required='true'");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Name * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tname","","class='form-control tname'true' data-parsley-required='false'");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Parent Code * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tparentid",$arrDept,"-","class='form-control tparentid' data-parsley-required='false'");?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<?php echo form_submit("btnSubmit","Save","class='btn btn-sm btn-primary'");?>
				</div>
            <?php echo form_close();?>
		</div>
	</div>
</div>

<!-- #modal-delete -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog">
		<div class="modal-content ">
			<?php echo form_open("$kelas/add","class='form-horizontal formDetail' data-parsley-validate='true'");?>
			<?php echo form_input("tid","","class='form-control tid hidden'");?>
				<div class="modal-header bg-red">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-white">Delete <?=$title?></h4>
				</div>
				<div class="modal-body">
					<h3 class="text-center m-t-10">Are you sure delete this data?</h3>
					<br>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Code * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tcode","","class='form-control tcode' data-parsley-required='true' disabled");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Name * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tname","","class='form-control tname'true' data-parsley-required='false' disabled");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Parent Code * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tparentid",$arrDept,"-","class='form-control tparentid' data-parsley-required='false' disabled");?>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<?php echo form_submit("btnSubmit","Yes","class='btn btn-sm btn-primary'");?>
				</div>
            <?php echo form_close();?>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(".getDetail").on("click",function(){
		var aksi = $(this).data("action");
		$(".tid").val($(this).data("id"));
		$(".tcode").val($(this).data("code"));
		$(".tname").val($(this).data("name"));
		$(".formDetail").attr("action","<?=site_url().$kelas?>/"+aksi);
	});
</script>