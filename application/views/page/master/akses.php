<?php
$flashdata = $this->session->flashdata();
$dbmenu = $this->M_menu->getAll();

$tmenu[""] = " - ";

foreach($dbmenu as $row){
   $tmenu[$row->menuid] = $row->name;
}

?>
<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="<?=site_url('')?>">Home</a></li>
	<li><a href="javascript:;">Document</a></li>
	<li class="active"><?=$title?></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header"><?=$title?></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">Form</h4>
            </div>
            <div class="panel-body" id="app">

                <?php if($this->session->flashdata("success")): ?>
                <div class="alert alert-success fade in m-b-15">
                    <strong>Success!</strong>
                    <?=$flashdata["success"];?>
                    <span class="close" data-dismiss="alert">×</span>
                </div>
                <?php elseif($this->session->flashdata("warning")): ?>
            	<div class="alert alert-warning fade in m-b-15">
                    <strong>Warning!</strong>
                    <?=$flashdata["warning"];?>
                    <span class="close" data-dismiss="alert">×</span>
                </div>
                <?php endif; ?>

                <?=form_open("", "class='form-horizontal'");?>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Username</label>
                        <div class="col-md-8">
                            <?=form_input("",$userData->username,"class='form-control' readonly" ); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Nickname</label>
                        <div class="col-md-8">
                            <?=form_input("",$userData->nickname,"class='form-control' readonly" ); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Fullname</label>
                        <div class="col-md-8">
                            <?=form_input("",$userData->fullname,"class='form-control' readonly" ); ?>
                        </div>
                    </div>
                </div>
                <?=form_close(); ?>

                <?=form_open("Master/AksesUserMenu/add", "class='form-horizontal'");?>
                <?=form_hidden("tuserid",$userData->userid,"class='form-control'" ); ?>

                    <!-- LINKED TO -->
                    <div class="col-md-6">
                        
                        <h5><strong> <i class="fa fa-link"></i> Linked to</strong></h5>
                        <?php if(count($rowMenu)):?>
                        <div class="form-group">
                            <table id="data-table" class="table table-striped table-bordered">
                                <?php foreach($rowMenu as $item):?>
                                <tr>
                                    <td><?=$this->M_menu->getDetail($item->menuid)->name;?></td>
                                    <td width="5%">
                                        <a class="btn btn-danger btn-xs" href="<?=site_url('Master/AksesUserMenu/delete/').$item->usermenuid;?>" data-toggle="tooltip" title="delete"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach;?>
                            </table>
                        </div>
                        <?php endif; ?>
                        <div class="form-group">
                            <label class="col-md-1 control-label">Menu</label>
                            <div class="col-md-11">
                                <select class='selectpicker form-control' data-live-search='true' name="tmenuid">
                                    <option value="">-</option>
                                <?php
                                // MENU 1
                                    $rowMenu1 = $this->M_menu->getAllBy("level = 1");
                                    foreach ($rowMenu1 as $menu1):
                                      if($this->M_menu->getAllBy("parentid = ".$menu1->menuid)):
                                ?>
                                  <optgroup label="<?=$menu1->name;?>">

                                    <?php
                                    // MENU 2
                                        $rowMenu2 = $this->M_menu->getAllBy("level = 2 AND parentid = $menu1->menuid");
                                        foreach ($rowMenu2 as $menu2):
                                          if($this->M_menu->getAllBy("parentid = ".$menu2->menuid)):
                                    ?>
                                      <optgroup label="-- <?=$menu2->name;?>">

                                        <?php
                                        // MENU 3
                                            $rowMenu3 = $this->M_menu->getAllBy("level = 3 AND parentid = $menu2->menuid");
                                            foreach ($rowMenu3 as $menu3):
                                              if($this->M_menu->getAllBy("parentid = ".$menu3->menuid)):
                                        ?>
                                          <optgroup label="--- <?=$menu3->name;?>">

                                            <?php
                                            // MENU 4
                                                $rowMenu4 = $this->M_menu->getAllBy("level = 4 AND parentid = $menu3->menuid");
                                                foreach ($rowMenu4 as $menu4):
                                                  if($this->M_menu->getAllBy("parentid = ".$menu4->menuid)):
                                            ?>
                                              <optgroup label="---- <?=$menu4->name;?>">

                                                <?php
                                                // MENU 5
                                                    $rowMenu5 = $this->M_menu->getAllBy("level = 5 AND parentid = $menu4->menuid");
                                                    foreach ($rowMenu5 as $menu5):
                                                ?>
                                                    <option value="<?=$menu5->menuid;?>">----- <?=$menu5->name;?></option>
                                                <?php
                                                    endforeach;
                                                ?>
                                              </optgroup>
                                            <?php else: ?>
                                                <option value="<?=$menu4->menuid;?>">---- <?=$menu4->name;?></option>
                                            <?php endif; ?>
                                            <?php
                                                endforeach;
                                            ?>
                                          </optgroup>
                                        <?php else: ?>
                                            <option value="<?=$menu3->menuid;?>">--- <?=$menu3->name;?></option>
                                        <?php endif; ?>
                                        <?php
                                            endforeach;
                                        ?>
                                      </optgroup>
                                    <?php else: ?>
                                        <option value="<?=$menu2->menuid;?>">-- <?=$menu2->name;?></option>
                                    <?php endif; ?>
                                    <?php
                                        endforeach;
                                    ?>
                                  </optgroup>
                                <?php else: ?>
                                    <option value="<?=$menu1->menuid;?>"><?=$menu1->name;?></option>
                                <?php endif; ?>
                                <?php
                                    endforeach;
                                ?>
                                </select>

<!--                                 <?=form_dropdown("dupmenuid",$tmenu,$data->menuid,"id='menuid' class='selectpicker form-control' data-live-search='true'" ); ?> -->
                            </div>
                        </div>                    
                        <div class="form-group">
                            <div class="col-md-12">
                                <button name="btnSubmit" type="submit" class="btn btn-sm btn-success m-r-5 pull-right" value=true>+</button>
                            </div>
                        </div>
                    </div>
                <?=form_close();?>
                 
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->

<script>
    // var app = new Vue({
    //     el: '#app',
    //     data: {
    //         menuVal: "",
    //         <?php if($data->code != ""): ?>
    //         showDept: 1,
    //         <?php else: ?>
    //         showDept: 0,
    //         <?php endif; ?>
    //     },
    //     watch: {
    //         menuVal: function(val, oldval){
    //             this.showDept = 0;
    //             var url = "<?=site_url('Api/Menu/index_get/');?>" + val;
    //             this.$http.get(url).then( function(resp){
    //                 console.log(resp.data);
    //                 if(resp.data.categoryid == 2){
    //                     this.showDept = 1;
    //                 }else{
    //                     $("#deptid").val("");
    //                 }
    //                 console.log(val, oldval);
    //             }).catch(function(err){
    //                 console.log(err);
    //             });
    //         }
    //     }
    // });
    </script>
