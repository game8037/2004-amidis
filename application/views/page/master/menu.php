<?php

$arrMenu[""] = "== Pilih $title ==";  
foreach ($rowData as $row) {
    $arrMenu[$row->menuid] = $row->name;  
}

$arrCategory[""] = "== Pilih Category ==";  
$rowCategory = $this->M_category->getAll();
foreach ($rowCategory as $row) {
    $arrCategory[$row->categoryid] = $row->name;  
}
?>



<!-- begin breadcrumb -->
<ol class="breadcrumb pull-right">
	<li><a href="<?=site_url('')?>">Home</a></li>
	<li><a href="javascript:;">Master Data</a></li>
	<li class="active"><?=$title?></li>
</ol>
<!-- end breadcrumb -->
<!-- begin page-header -->
<h1 class="page-header"><?=$title?></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-12 -->
    <div class="col-md-12">
        <!-- begin panel -->
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <div class="panel-heading-btn">
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                </div>
                <h4 class="panel-title">List</h4>
            </div>
            <div class="panel-body">
			    <div>
						<a class="btn btn-primary" href="#modal-add" data-toggle="modal">
							<i class="fa fa-plus"></i>
							<span>Add New</span>
						</a>
						<div>&nbsp;</div>
				</div>
                <div class="table-responsive">
                    <table id="data-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
				              <th width="10%">No</th>
				              <th>Level 1</th>
				              <th>Level 2</th>
				              <th>Level 3</th>
				              <th>Level 4</th>
				              <th>Level 5</th>
				              <!-- <th>Category</th> -->
				              <th width="15%">action</th>
                            </tr>
                        </thead>
                        <tbody>
						  <?php         
						  $no = 1;
                            // MENU 1
                                $rowMenu1 = $this->M_menu->getAllBy("level = 1");
                                foreach ($rowMenu1 as $menu1):
                            ?>
                              <tr>
                              	<td><?=$no++?></td>
                              	<td><?=$menu1->name;?> </td>
                              	<td></td>
                              	<td></td>
                              	<td></td>
                              	<td></td>                              	
				                <td class="text-center">
					                <span data-toggle="tooltip" data-placement="top" title="edit">
					                  <a href="#modal-detail" class="btn btn-xs btn-warning getDetail" data-toggle="modal" data-action="update" data-id="<?=$menu1->menuid;?>" data-name="<?=$menu1->name;?>" data-parentid="<?=$menu1->parentid;?>" data-categoryid="<?=$menu1->categoryid;?>"><i class="fa fa-pencil"></i> </a>
					                </span>
					                <span data-toggle="tooltip" data-placement="top" title="delete">
					                  <a href="#modal-delete" class="btn btn-xs btn-danger getDetail" data-toggle="modal" data-action="delete" data-id="<?=$menu1->menuid;?>" data-name="<?=$menu1->name;?>" data-parentid="<?=$menu1->parentid;?>" data-categoryid="<?=$menu1->categoryid;?>"> <i class="fa fa-trash"></i> </a>
					                </span>
				                </td>
                              </tr>

                                <?php
                                // MENU 2
                                    $rowMenu2 = $this->M_menu->getAllBy("level = 2 AND parentid = $menu1->menuid");
                                    foreach ($rowMenu2 as $menu2):
                                ?>
                                  <tr>
                                  	<td><?=$no++?></td>
	                              	<td><?=$menu1->name;?> </td>
	                              	<td><?=$menu2->name;?> </td>
	                              	<td></td>
	                              	<td></td>
	                              	<td></td>
					                <td class="text-center">
						                <span data-toggle="tooltip" data-placement="top" title="edit">
						                  <a href="#modal-detail" class="btn btn-xs btn-warning getDetail" data-toggle="modal" data-action="update" data-id="<?=$menu2->menuid;?>" data-name="<?=$menu2->name;?>" data-parentid="<?=$menu2->parentid;?>" data-categoryid="<?=$menu2->categoryid;?>"><i class="fa fa-pencil"></i> </a>
						                </span>
						                <span data-toggle="tooltip" data-placement="top" title="delete">
						                  <a href="#modal-delete" class="btn btn-xs btn-danger getDetail" data-toggle="modal" data-action="delete" data-id="<?=$menu2->menuid;?>" data-name="<?=$menu2->name;?>" data-parentid="<?=$menu2->parentid;?>" data-categoryid="<?=$menu2->categoryid;?>"> <i class="fa fa-trash"></i> </a>
						                </span>
					                </td>
                                  </tr>

                                    <?php
                                    // MENU 3
                                        $rowMenu3 = $this->M_menu->getAllBy("level = 3 AND parentid = $menu2->menuid");
                                        foreach ($rowMenu3 as $menu3):
                                    ?>
                                      <tr>
                                      	<td><?=$no++?></td>
		                              	<td><?=$menu1->name;?> </td>
		                              	<td><?=$menu2->name;?> </td>
                                      	<td><?=$menu3->name;?> </td>
		                              	<td></td>
		                              	<td></td>
						                <td class="text-center">
							                <span data-toggle="tooltip" data-placement="top" title="edit">
							                  <a href="#modal-detail" class="btn btn-xs btn-warning getDetail" data-toggle="modal" data-action="update" data-id="<?=$menu3->menuid;?>" data-name="<?=$menu3->name;?>" data-parentid="<?=$menu3->parentid;?>" data-categoryid="<?=$menu3->categoryid;?>"><i class="fa fa-pencil"></i> </a>
							                </span>
							                <span data-toggle="tooltip" data-placement="top" title="delete">
							                  <a href="#modal-delete" class="btn btn-xs btn-danger getDetail" data-toggle="modal" data-action="delete" data-id="<?=$menu3->menuid;?>" data-name="<?=$menu3->name;?>" data-parentid="<?=$menu3->parentid;?>" data-categoryid="<?=$menu3->categoryid;?>"> <i class="fa fa-trash"></i> </a>
							                </span>
						                </td>
                                      </tr>

                                        <?php
                                        // MENU 4
                                            $rowMenu4 = $this->M_menu->getAllBy("level = 4 AND parentid = $menu3->menuid");
                                            foreach ($rowMenu4 as $menu4):
                                        ?>
                                          <tr>
                                          	<td><?=$no++?></td>
			                              	<td><?=$menu1->name;?> </td>
			                              	<td><?=$menu2->name;?> </td>
	                                      	<td><?=$menu3->name;?> </td>
                                          	<td><?=$menu4->name;?> </td>
			                              	<td></td>
							                <td class="text-center">
								                <span data-toggle="tooltip" data-placement="top" title="edit">
								                  <a href="#modal-detail" class="btn btn-xs btn-warning getDetail" data-toggle="modal" data-action="update" data-id="<?=$menu4->menuid;?>" data-name="<?=$menu4->name;?>" data-parentid="<?=$menu4->parentid;?>" data-categoryid="<?=$menu4->categoryid;?>"><i class="fa fa-pencil"></i> </a>
								                </span>
								                <span data-toggle="tooltip" data-placement="top" title="delete">
								                  <a href="#modal-delete" class="btn btn-xs btn-danger getDetail" data-toggle="modal" data-action="delete" data-id="<?=$menu4->menuid;?>" data-name="<?=$menu4->name;?>" data-parentid="<?=$menu4->parentid;?>" data-categoryid="<?=$menu4->categoryid;?>"> <i class="fa fa-trash"></i> </a>
								                </span>
							                </td>
                                          </tr>

                                            <?php
                                            // MENU 5
                                                $rowMenu5 = $this->M_menu->getAllBy("level = 5 AND parentid = $menu4->menuid");
                                                foreach ($rowMenu5 as $menu5):
                                            ?>
                                          		<tr>
                                          			<td><?=$no++?></td>
					                              	<td><?=$menu1->name;?> </td>
					                              	<td><?=$menu2->name;?> </td>
			                                      	<td><?=$menu3->name;?> </td>
		                                          	<td><?=$menu4->name;?> </td>
                                          			<td><?=$menu5->name;?> </td>
									                <td class="text-center">
										                <span data-toggle="tooltip" data-placement="top" title="edit">
										                  <a href="#modal-detail" class="btn btn-xs btn-warning getDetail" data-toggle="modal" data-action="update" data-id="<?=$menu5->menuid;?>" data-name="<?=$menu5->name;?>" data-parentid="<?=$menu5->parentid;?>" data-categoryid="<?=$menu5->categoryid;?>"><i class="fa fa-pencil"></i> </a>
										                </span>
										                <span data-toggle="tooltip" data-placement="top" title="delete">
										                  <a href="#modal-delete" class="btn btn-xs btn-danger getDetail" data-toggle="modal" data-action="delete" data-id="<?=$menu5->menuid;?>" data-name="<?=$menu5->name;?>" data-parentid="<?=$menu5->parentid;?>" data-categoryid="<?=$menu5->categoryid;?>"> <i class="fa fa-trash"></i> </a>
										                </span>
									                </td>
                                          		</tr>
                                            <?php
                                                endforeach;
                                            endforeach;
                                        endforeach;
                                    endforeach;
                                endforeach;
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- end panel -->
    </div>
    <!-- end col-12 -->
</div>
<!-- end row -->





<!-- #modal-add -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog">
		<div class="modal-content">
			<?php echo form_open("$kelas/add","class='form-horizontal' data-parsley-validate='true'");?>
				<div class="modal-header bg-blue">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-white">Add <?=$title?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Name * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tname","","class='form-control' data-parsley-required='true'");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Parent Code * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tparentid",$arrMenu,"-","class='form-control tparentid selectpicker' data-size='10' data-live-search='true' data-style='btn-white'");?>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Category * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tcategoryid",$arrCategory,"-","class='form-control tcategoryid' data-parsley-required='true'");?>
						</div>
					</div> -->
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<?php echo form_submit("btnSubmit","SUBMIT","class='btn btn-sm btn-primary'");?>
				</div>
            <?php echo form_close();?>
		</div>
	</div>
</div>

<!-- #modal-detail -->
<div class="modal fade" id="modal-detail">
	<div class="modal-dialog">
		<div class="modal-content">
			<?php echo form_open("$kelas/add","class='form-horizontal formDetail' data-parsley-validate='true'");?>
			<?php echo form_input("tid","","class='form-control tid hidden'");?>
				<div class="modal-header bg-orange">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-white">Update <?=$title?></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Name * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tname","","class='form-control tname'true' data-parsley-required='true'");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Parent Code * :</label>
                        <div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tparentid",$arrMenu,"-","class='form-control tparentid selectpicker' data-size='10' data-live-search='true' data-style='btn-white'");?>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Category * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tcategoryid",$arrCategory,"-","class='form-control tcategoryid' data-parsley-required='true'");?>
						</div>
					</div> -->
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<?php echo form_submit("btnSubmit","Save","class='btn btn-sm btn-primary'");?>
				</div>
            <?php echo form_close();?>
		</div>
	</div>
</div>

<!-- #modal-delete -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog">
		<div class="modal-content ">
			<?php echo form_open("$kelas/add","class='form-horizontal formDetail' data-parsley-validate='true'");?>
			<?php echo form_input("tid","","class='form-control tid hidden'");?>
				<div class="modal-header bg-red">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title text-white">Delete <?=$title?></h4>
				</div>
				<div class="modal-body">
					<h3 class="text-center m-t-10">Are you sure delete this data?</h3>
					<br>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Name * :</label>
						<div class="col-md-6 col-sm-6">
							<?php echo form_input("tname","","class='form-control tname'true' data-parsley-required='false' disabled");?>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Parent Code * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tparentid",$arrMenu,"-","class='form-control tparentid' data-parsley-required='false' disabled");?>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="control-label col-md-4 col-sm-4" for="fullname">Category * :</label>
						<div class="col-md-6 col-sm-6">
                            <?php echo form_dropdown("tcategoryid",$arrCategory,"-","class='form-control tcategoryid' data-parsley-required='false' disabled");?>
						</div>
					</div> -->
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
					<?php echo form_submit("btnSubmit","Yes","class='btn btn-sm btn-primary'");?>
				</div>
            <?php echo form_close();?>
		</div>
	</div>
</div>


<script type="text/javascript">
	$(".getDetail").on("click",function(){
		var aksi = $(this).data("action");
		$(".tid").val($(this).data("id"));
		$(".tname").val($(this).data("name"));
		$(".tparentid").val($(this).data("parentid"));
		$(".tcategoryid").val($(this).data("categoryid"));
		$(".formDetail").attr("action","<?=site_url().$kelas?>/"+aksi);
	});
</script>