<?php
$rowMenu1 = $this->M_menu->getAllBy("level = 1");
$rowDept = $this->M_department->getAll();
$iduser = $this->session->userdata("id");
$user = $this->M_user->getDetail($iduser);
?>

<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <div class="image">
                    <a href="javascript:;"><img src="<?=base_url('extras/');?>/img/propil.jpg" alt="" /></a>
                </div>
                <div class="info">
                    <?=$user->fullname?>
                    <small><?=$user->nickname?></small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->
        <ul class="nav">
            <li class="nav-header">Navigation</li>
            <li class="<?=($this->uri->segment(1) == '')?'active':''?>">
                <a href="<?=site_url('')?>"><i class="fa fa-laptop"></i> <span>Dashboard</span></a>
            </li>

            <li class="<?=($this->uri->segment(2) == 'Upload')?'active':''?>">
                <a href="<?=site_url('Document/Upload/')?>"><i class="fa fa-upload"></i> <span>Upload Master</span></a>
            </li>
            <li class="<?=($this->uri->segment(2) == 'Document')?'active':''?>">
                <a href="<?=site_url('Document/Display/Menu/0')?>"><i class="fa fa-folder"></i> <span>TEMPORARY FOLDER</span></a>
            </li>
            <?php if($user->roleid == 1):?>
            <li class="has-sub <?=($this->uri->segment(1) == 'Master')?'active':''?>">
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-gear"></i>
                    <span>Master Data</span>
                </a>
                <ul class="sub-menu">
                    <li class="<?=($this->uri->segment(2) == 'Department')?'active':''?>"><a href="<?=site_url('Master/Department/')?>">Department</a></li>
                    <li class="<?=($this->uri->segment(2) == 'Menu')?'active':''?>"><a href="<?=site_url('Master/Menu/')?>">Menu</a></li>
                    <li class="<?=($this->uri->segment(2) == 'User')?'active':''?>"><a href="<?=site_url('Master/User/')?>">User</a></li>
                </ul>
            </li>
            <?php endif;?>
            <!-- MENU 1 -->
            <?php
            foreach ($rowMenu1 as $menu1):
                // akses level 1
                if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu1->menuid")) != null || $user->roleid == 1):
                    if($this->M_menu->getAllBy("parentid = ".$menu1->menuid)):
                        ?>
                        <li class="has-sub">
                            <a href="javascript:;">
                                <b class="caret pull-right"></b>
                                <i class="fa fa-folder-o"></i>
                                <span><?=$menu1->name;?></span>
                            </a>
                            <ul class="sub-menu">
                                <!-- MENU 2 -->
                                <?php
                                $rowMenu2 = $this->M_menu->getAllBy("level = 2 AND parentid = $menu1->menuid");
                                foreach ($rowMenu2 as $menu2):
                                    // akses level 2
                                    if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu2->menuid")) != null || $user->roleid == 1):
                                        if($this->M_menu->getAllBy("parentid = ".$menu2->menuid)):
                                            ?>
                                            <li class="has-sub">
                                                <a href="javascript:;">
                                                    <b class="caret pull-right"></b>
                                                    <span><?=$menu2->name;?></span>
                                                </a>
                                                <ul class="sub-menu">
                                                    <!-- MENU 3-->
                                                    <?php
                                                    $rowMenu3 = $this->M_menu->getAllBy("level = 3 AND parentid = $menu2->menuid");
                                                    foreach ($rowMenu3 as $menu3):
                                                        // akses level 3
                                                        if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu3->menuid")) != null || $user->roleid == 1):
                                                            if($this->M_menu->getAllBy("parentid = ".$menu3->menuid)):
                                                                ?>
                                                                <li class="has-sub">
                                                                    <a href="javascript:;">
                                                                        <b class="caret pull-right"></b>
                                                                        <span><?=$menu3->name;?></span>
                                                                    </a>
                                                                    <ul class="sub-menu">
                                                                        <!-- MENU 4-->
                                                                        <?php
                                                                        $rowMenu4 = $this->M_menu->getAllBy("level = 4 AND parentid = $menu3->menuid");
                                                                        foreach ($rowMenu4 as $menu4):
                                                                            // akses level 4
                                                                            if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu4->menuid")) != null || $user->roleid == 1):
                                                                                if($this->M_menu->getAllBy("parentid = ".$menu4->menuid)):
                                                                                    ?>
                                                                                    <li class="has-sub">
                                                                                        <a href="javascript:;">
                                                                                            <b class="caret pull-right"></b>
                                                                                            <span><?=$menu4->name;?></span>
                                                                                        </a>
                                                                                        <ul class="sub-menu">
                                                                                            <!-- MENU 5-->
                                                                                            <?php
                                                                                            $rowMenu5 = $this->M_menu->getAllBy("level = 5 AND parentid = $menu4->menuid");
                                                                                            foreach ($rowMenu5 as $menu5):
                                                                                                // akses level 5
                                                                                                if(($this->M_akses->getByQuery("userid = $iduser AND menu = $menu5->menuid")) != null || $user->roleid == 1):
                                                                                                ?>
                                                                                                <li class="">
                                                                                                    <a href="<?=site_url('Document/Display/Menu/'.$menu5->menuid)?>"><span><?=$menu5->name?></span></a>
                                                                                                </li>
                                                                                            <?php
                                                                                                endif;
                                                                                            endforeach; ?>
                                                                                            <!-- END MENU 5 -->
                                                                                        </ul>
                                                                                    </li>
                                                                                <?php
                                                                                else:
                                                                                    ?>
                                                                                    <li class="">
                                                                                        <a href="<?=site_url('Document/Display/Menu/'.$menu4->menuid)?>"><span><?=$menu4->name?></span></a>
                                                                                    </li>
                                                                                <?php
                                                                                endif;
                                                                            endif;
                                                                        endforeach; ?>
                                                                        <!-- END MENU 4 -->
                                                                    </ul>
                                                                </li>
                                                            <?php
                                                            else:
                                                                ?>
                                                                <li class="">
                                                                    <a href="<?=site_url('Document/Display/Menu/'.$menu3->menuid)?>"><span><?=$menu3->name?></span></a>
                                                                </li>
                                                            <?php
                                                            endif;
                                                        endif;
                                                    endforeach; ?>
                                                    <!-- END MENU 3 -->
                                                </ul>
                                            </li>
                                        <?php
                                        else:
                                            ?>
                                            <li class="">
                                                <a href="<?=site_url('Document/Display/Menu/'.$menu2->menuid)?>"><span><?=$menu2->name?></span></a>
                                            </li>
                                        <?php
                                        endif;
                                    endif;
                                endforeach; ?>
                                <!-- END MENU 2 -->
                            </ul>
                        </li>
                    <?php
                    else:
                        ?>
                        <li class="">
                            <a href="<?=site_url('Document/Display/Menu/'.$menu1->menuid)?>"><i class="fa fa-folder-o"></i> <span><?=$menu1->name?></span></a>
                        </li>
                    <?php
                    endif;
                endif;
            endforeach; ?>
            <!-- END MENU 1 -->


            <!-- EKSTERNAL -->
            <!-- MENU EKSTERNAL -->
            <?php
            // foreach ($rowDept as $dept):
            ?>
            <!-- <li class="has-sub">
        <a href="javascript:;">
            <b class="caret pull-right"></b>
            <i class="fa fa-folder"></i>
            <span>Data <?=$dept->code;?></span>
          </a>
        <ul class="sub-menu">
          <li class="">
            <a href="<?=site_url('Document/Display/Dept/sop/'.$dept->deptid)?>"><span>SOP</span></a>
            <a href="<?=site_url('Document/Display/Dept/doc/'.$dept->deptid)?>"><span>Dokumentasi</span></a>
            <a href="<?=site_url('Document/Display/Dept/dta/'.$dept->deptid)?>"><span>Data</span></a>
          </li>
        </ul> -->
            <?php
            // endforeach;
            ?>
            <!-- END MENU EKSTERNAL -->
            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>